# PLEASE NOTE!

## Requirement

This project depends on my Master POM, which can be pulled from:
https://gitlab.com/jamesap/MasterPom.git

Please download and install the MasterPom project for this project to work.