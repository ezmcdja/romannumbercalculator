package com.ezmcdja.calculator.calculation;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: James McDermott.
 */
public enum RomanNumeral {
    I(1, "I"), IV(4, "IV"), V(5, "V"), IX(9, "IX"), X(10, "X"), XL(40, "XL"), L(50, "L"), XC(90, "XC"), C(100,
            "C"), CD(400, "CD"), D(500, "D"), CM(900, "CM"), M(1000, "M");

    private Integer decimalVersion;
    private String romanVersion;

    RomanNumeral(final Integer decimalVersion, final String romanVersion) {
        this.decimalVersion = decimalVersion;
        this.romanVersion = romanVersion;
    }

    public static List<String> getBasicNumerals() {
        final List<String> numerals = new ArrayList<>();
        for (final RomanNumeral each : RomanNumeral.values()) {
            if (each.getRoman().length() == 1) {
                numerals.add(0, each.getRoman());
            }
        }
        return numerals;
    }

    public String getRoman() {
        return this.romanVersion;
    }

    Integer getDecimal() {
        return this.decimalVersion;
    }

    static Integer parseNumeralToDecimal(final String numeral) {
        for (final RomanNumeral each : RomanNumeral.values()) {
            if (each.romanVersion.equals(numeral)) {
                return each.decimalVersion;
            }
        }
        return 0;
    }
}
