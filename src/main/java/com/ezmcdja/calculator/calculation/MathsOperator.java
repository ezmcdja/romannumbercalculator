package com.ezmcdja.calculator.calculation;


import java.util.ArrayList;
import java.util.List;

/**
 * Author: James McDermott.
 *
 */
public enum MathsOperator {
    HASH_END("#", 0) {
        public Integer[] evaluate(final Integer operand1, final Integer operand2) {
            return new Integer[] {};
        }
    },
    PLUS("+", 1) {
        public Integer[] evaluate(final Integer operand1, final Integer operand2) {
            return new Integer[] {operand1 + operand2, 0};
        }
    },
    MINUS("-", 1) {
        public Integer[] evaluate(final Integer operand1, final Integer operand2) {
            return new Integer[] {operand1 - operand2, 0};
        }
    },
    TIMES("x", 2) {
        public Integer[] evaluate(final Integer operand1, final Integer operand2) {
            return new Integer[] {operand1 * operand2, 0};
        }
    },
    DIVIDE("÷", 2) {
        public Integer[] evaluate(final Integer operand1, final Integer operand2) {
            return new Integer[] {operand1 / operand2, operand1 % operand2};
        }
    };

    private final Integer precedence;
    private final String sign;

    MathsOperator(final String sign, final Integer precedence) {
        this.sign = sign;
        this.precedence = precedence;
    }

    public static List<String> getOperatorSigns() {
        final List<String> signs = new ArrayList<>();
        for (final MathsOperator each : MathsOperator.values()) {
            if (!"#".equals(each.sign)) {
                signs.add(each.sign);
            }
        }
        return signs;
    }

    public Integer getPrecedence() {
        return this.precedence;
    }

    public static boolean isOperator(final String sign) {
        return getOperatorSigns().contains(sign);
    }

    public static MathsOperator parseSignToOperator(final String sign) {
        for (final MathsOperator each : MathsOperator.values()) {
            if (sign == null) {
                break;
            }
            if (sign.equals(each.sign)) {
                return each;
            }
        }
        return HASH_END;
    }

    public abstract Integer[] evaluate(Integer operand1, Integer operand2);
}
