package com.ezmcdja.calculator.calculation;

import static com.ezmcdja.calculator.calculation.MathsOperator.HASH_END;
import static com.ezmcdja.calculator.calculation.MathsOperator.isOperator;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Author: James McDermott.
 */
public class RomanCalculation {
    private final StringBuilder decimalCalculation;
    private String romanResult;

    private final NumberConverter converter;
    private final RomanNumberValidator validator;

    private final Deque<String> reversePolishInput;
    private final Deque<Integer> operands;

    public RomanCalculation() {
        decimalCalculation = new StringBuilder("(");
        converter = new NumberConverter();
        validator = new RomanNumberValidator(converter);

        reversePolishInput = new ArrayDeque<>();
        operands = new ArrayDeque<>();
    }

    public void calculate(final String infixString) {
        final String[] operatorsAndOperands = infixString.split(" ");
        validateRomanOperands(operatorsAndOperands);
        buildDecimalCalculation(operatorsAndOperands);

        convertToReversePolish(operatorsAndOperands);

        try {
            this.romanResult = romanEvaluate(reversePolishInput);
        } catch (final NoSuchElementException e) {
            this.romanResult = infixString;
            this.decimalCalculation.append(converter.parseToDecimal(romanResult) + ")");
        }
    }

    public String getDecimalCalculation() {
        return decimalCalculation.toString();
    }

    public String getRomanResult() {
        return romanResult;
    }

    private void buildDecimalCalculation(final String[] operatorsAndOperands) {
        for (final String eachToken : operatorsAndOperands) {
            final String toAppend;
            if (isOperator(eachToken)) {
                toAppend = eachToken;
            } else {
                toAppend = converter.parseToDecimal(eachToken).toString();
            }
            decimalCalculation.append(toAppend + " ");
        }
        decimalCalculation.append("= ");
    }

    private void convertToReversePolish(final String[] infixInput) {
        final List<String> operatorsAndOperands = new ArrayList<>(Arrays.asList(infixInput));
        final Deque<String> operators = new ArrayDeque<>();

        operators.addFirst("#");
        while (!operatorsAndOperands.isEmpty()) {
            moveTokenToQueue(operatorsAndOperands, operators);
        }
        while (!"#".equals(operators.getFirst())) {
            reversePolishInput.addLast(operators.removeFirst());
        }
    }

    private void moveTokenToQueue(final List<String> operatorsAndOperands, final Deque<String> operators) {
        final String token = operatorsAndOperands.remove(0);
        final MathsOperator currentOperator = MathsOperator.parseSignToOperator(token);

        if (!isOperator(token)) {
            reversePolishInput.addLast(token);
        } else {
            moveOperandToQueue(operators, currentOperator);
            operators.addFirst(token);
        }
    }

    private void moveOperandToQueue(final Deque<String> operators, final MathsOperator currentOperator) {
        String topOfOperatorQueue = operators.peekFirst();
        MathsOperator previousOperator = MathsOperator.parseSignToOperator(topOfOperatorQueue);
        while (currentOperator.getPrecedence() <= previousOperator.getPrecedence() && !"#".equals(operators.peekFirst())) {
            reversePolishInput.addLast(operators.removeFirst());
            topOfOperatorQueue = operators.peekFirst();
            previousOperator = MathsOperator.parseSignToOperator(topOfOperatorQueue);
        }
    }

    private Integer[] performOperation(final Integer operand1, final MathsOperator operator, final Integer operand2) {
        if (operator == HASH_END) {
            throw new IllegalArgumentException("Unrecognised operator. How did you even do that?");
        }
        final Integer[] result = operator.evaluate(operand1, operand2);
        if (result[0] == 0) {
            throw new AnachronisticZeroException("ZERO RESULT! ROME PANICS!");
        }
        return result;
    }

    private String romanEvaluate(final Deque<String> reversePolishInput) {
        Integer remainder = 0;
        while (!reversePolishInput.isEmpty()) {
            remainder = performOperations(reversePolishInput, remainder);
        }
        decimalCalculation.append(operands.peekLast() + ")");
        if (remainder != 0) {
            decimalCalculation.append(" (Remainder: " + remainder + ")");
        }
        return converter.convertToRoman(operands.peekLast());
    }

    private Integer performOperations(final Deque<String> reversePolishInput, Integer remainder) {
        moveOperandsToOperandQueue(reversePolishInput);

        final Integer[] resultArray;
        Integer result = 0;

        try {
            resultArray = constructAndPerformOperation(reversePolishInput);
            result = resultArray[0];
            remainder = resultArray[1];

            operands.addLast(result);
        } catch (final AnachronisticZeroException aze) {
            checkIfZeroIsPartOfCalculation(reversePolishInput, aze);
        }
        return remainder;
    }

    private void moveOperandsToOperandQueue(final Deque<String> reversePolishInput) {
        while (!isOperator(reversePolishInput.peekFirst())) {
            operands.addLast(converter.parseToDecimal(reversePolishInput.removeFirst()));
        }
    }

    private void checkIfZeroIsPartOfCalculation(final Deque<String> reversePolishInput, final AnachronisticZeroException aze) {
        if (!reversePolishInput.isEmpty()) {
            operands.addLast(0);
        } else {
            throw new AnachronisticZeroException(aze.getMessage());
        }
    }

    private Integer[] constructAndPerformOperation(final Deque<String> reversePolishInput) {
        final Integer operand2 = operands.removeLast();
        final Integer operand1 = operands.removeLast();
        final MathsOperator operator = MathsOperator.parseSignToOperator(reversePolishInput.removeFirst());

        return performOperation(operand1, operator, operand2);
    }

    private void validateRomanOperands(final String[] operatorsAndOperands) {
        for (final String operand : operatorsAndOperands) {
            if (isOperator(operand)) {
                continue;
            }
            validator.validateRomanNumber(operand);
        }
    }
}
