package com.ezmcdja.calculator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ezmcdja.calculator.calculation.NumberConverter;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

/**
 *  Tests for {@link NumberConverter}.
 */
@RunWith(JUnitParamsRunner.class)
public class NumberConverterTest {
    private static final NumberConverter CONVERTER = new NumberConverter();

    private final Logger logger = LoggerFactory.getLogger(this.getClass().getSimpleName());

    @Test
    @Parameters({ "1, I", "4, IV", "5, V", "6, VI", "9, IX", "10, X", "14, XIV", "15, XV", "16, XVI", "19, XIX", "20, XX", "40, XL", "44, XLIV",
            "49, XLIX", "50, L", "54, LIV", "59, LIX", "60, LX", "70, LXX", "90, XC", "99, XCIX", "100, C", "109, CIX", "140, CXL", "149, CXLIX",
            "150, CL", "190, CXC", "199, CXCIX", "200, CC", "400, CD", "490, CDXC", "499, CDXCIX", "500, D", "540, DXL", "900, CM", "990, CMXC",
            "999, CMXCIX", "1000, M", "1968, MCMLXVIII", "8245, MMMMMMMMCCXLV" })
    public void testDecimalToRoman(final Integer number, final String roman) {
        assertEquals(roman, CONVERTER.convertToRoman(number));
        logger.info("{} converts to {}", number, roman);
    }

    @Test
    @Parameters({ "1, I", "4, IV", "5, V", "6, VI", "9, IX", "10, X", "14, XIV", "15, XV", "16, XVI", "19, XIX", "20, XX", "20, VVVV", "25, VVVVV",
            "40, XL", "44, XLIV", "49, XLIX", "50, L", "54, LIV", "59, LIX", "60, LX", "70, LXX", "90, XC", "99, XCIX", "100, C", "109, CIX",
            "140, CXL", "149, CXLIX", "150, CL", "190, CXC", "199, CXCIX", "200, CC", "400, CD", "490, CDXC", "499, CDXCIX", "500, D", "540, DXL",
            "900, CM", "990, CMXC", "999, CMXCIX", "1000, M", "1968, MCMLXVIII", "8245, MMMMMMMMCCXLV" })
    public void testRomanToDecimal(final Integer number, final String roman) {
        assertEquals(number, CONVERTER.parseToDecimal(roman));
        logger.info("{} converts to {}", roman, number);
    }
}
